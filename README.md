# llvm-kill-memory

This in an [LLVM pass](http://releases.llvm.org/3.8.1/docs/WritingAnLLVMPass.html) written for LLVM 3.8 
as part of my graduate studies.

It finds all the Store instructions in a portion of LLVM code and outputs the address in a form that is somewhat human readable.

# Building

Run the following commands to build the project.


    $ cd llvm-kill-memory
    $ mkdir build
    $ cd build
    $ LLVM_DIR=<path to LLVM 3.8 build>/share/llvm/cmake cmake ..
    $ make
    $ cd ..

# Running

    $ <path to LLVM 3.8 build>/bin/opt -load llvm-kill-memory/build/source/libKillMemoryPass.so -o /dev/null -killed-memory < <bitcode file> 2>&1
